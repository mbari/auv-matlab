function auv_navplot_hack(mission)
figure;
s.logname = {'ta.data', 'ta.data', 'ta.data', 'dataNav', 'dataNav', 'dataK', 'gps'};
s.xName = {'ta.navE', 'ta.mleE', 'ta.mmseE', 'mPos_y', 'mGpsEast', 'mEastK', 'longitude'};
s.yName = {'ta.navN', 'ta.mleN', 'ta.mmseN', 'mPos_x', 'mGpsNorth', 'mNorthK', 'latitude'};
s.marker = {'co', 'm.', 'g.', 'bx', 'r.', 'y.', 'kx'};
for i = 1:length(s.logname)
    plotit(mission, s.logname{i}, s.xName{i}, s.yName{i}, s.marker{i}, i)
    if i == 1
        hold('on')
    end
end
hold('off')
axis('equal')
grid('on')
legend(s.xName)
xlabel('Easting (m)')
ylabel('Northing (m)')
title(['AUV Track: ' mission.mission ' - UTM Zone 10N'])
end

function plotit(mission, logname, xName, yName, marker, hackFlag)
x = mission.datum(logname, xName);
y = mission.datum(logname, yName);
if ~isempty(x) && ~isempty(y)
    
    
    if strcmp(logname, 'ta.data')       % ta.data is in UTM Zone 10N
        north = y.data;
        east = x.data;
        east(east == 0) = NaN;
        north(north == 0) = NaN;
    elseif strcmp(logname, 'gps')  % Flip lon sign. Convert GPS to UTM Zone 10N. 
        lat = rad2deg(y.data);
        lon = -rad2deg(x.data);
        [east, north, z] = geo2utm(lon, lat);
        if (z ~= 10)
            warning('Whoops, UTM zone is not 10N');
        end
    else
        east = x.data;
        north = y.data;
    end
    try 
        plot(east, north, marker)
    catch
        % do nothing
    end
    
else
    warning('AUV-MISSION:auv_navplot:notfound', ...
        ['Did not find both "' xName '" and "' yName '" for log ' logname]);
end

end