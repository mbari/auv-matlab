function missionLogs = loadauvlogs(directory)
% LOADAUVLOGS - Load a directory of AUV logs into a data structure
%
% Typically, The logs from a single AUV mission are stored in a single directory.
% This function load all the AUV logs from a single directory into Matlab memory.
%
% Usage:
%   missionLogs = loadauvlogs(directory)
%
% Inputs:
%   directory = The AUV directory containing the logs to be loaded.
%
% Output:
%   missionLogs = A containers.Map object where the keys are the shortened names
%           of the logs files (e.g. The key for the file 'gps.log' would be just
%           'gps'). The values are structures that contain the logs data. 
%
% See also readauvlog

% Brian Schlining
% 2013-04-25

logs = dir2(fullfile(directory, '*.log'));
if isempty(logs)
    warning(['No AUV logs were found in ' directory])
end

missionLogs = containers.Map;
for f = logs
    l = fullfile(directory, char(f));
    fprintf(1, 'Reading %s\n', l)
    try
        v = readauvlog(l);
        n = stringtokenizer(char(f), 1, '.');
        missionLogs(n) = v;
    catch 
        warning(['Failed to read ' l])
    end
end

