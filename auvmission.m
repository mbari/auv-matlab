% AUVMISSION - Object for easy access to AUV log data
classdef auvmission < handle
    
    properties
        mission = ''; % Can be anything but defaults to directory path of mission
        notes = {};   % Cell array that can be used to stuff notes
    end
    
    properties (SetAccess = private)
        logmap  % This is a map used to store auv data (key = log name without log, value = data struct)
    end
    
    methods
        
        %%
        function obj = auvmission(directory)
            %  AUVMISSION - constructor
            %
            % Usage:
            %  m = auvmission(directory)
            %  m = auvmission(containerMap)
            %
            if (ischar(directory))
                obj.mission = GetFullPath(directory);
                obj.logmap = loadauvlogs(directory);
            elseif isa(directory, 'containers.Map')
                % Create a auvmission using a logmap
                obj.logmap = directory;
            else
                me = MException('AUV_MATLAB:auvmission:illegalArgument', ...
                    'IllegalArgument passed to constructor');
            end
        end
        
        
        %%
        function s = log(obj, logName)
            % LOG - Grab the entire data structure for a specific log
            %
            % Usage:
            %   m = auvmission(directory)
            %   s = m.log(logName)
            %
            % Inputs:
            %   logMap = The container.Map object created by loadauvlogs
            %
            %   logName = The name of the log from which a variable shoud be extracted. This
            %             should be one of the values found in 'logMap.keys'. You can
            %             also use the instrument name. For example, you can get
            %             dynamic control data using either 'dynamicControl' or
            %             'dataDC' as the logName.
            %
            % Output:
            %   s = A data structure containing all variables that were read from a
            %       specific log. The following 2 examples should give identical results:
            %
            %       % Example 1
            %       m = auvmission(directory)
            %       s1 = m.log('dynamicControl')
            %
            %       % Example 2
            %       s2 = readauvlog(fullfile(somedir, 'dynamicControl.log'));
            %       % s1 and s2 should be identical
            k = obj.logname2mapkey(logName);
            s = struct([]);
            % 1st Look for 'logName' using the name of the logs.
            if ~isempty(k)
                s = obj.logmap(k);
            end
        end
        
        %%
        function v = data(obj, logName, varName)
            % DATA - Find variables from logs loaded into Matlab
            %
            % Searches a map loaded by loadauvlogs for specific variables and returns
            % the data array
            %
            % Usage:
            %   v = m.data(logName, varName)
            %
            %   logName = The name of the log from which a variable shoud be extracted. This
            %             should be one of the values found in 'logMap.keys'. You can
            %             also use the instrument name. For example, you can get
            %             dynamic control data using either 'dynamicControl' or
            %             'dataDC' as the logName.
            %
            %   varName = The name of the variable. e.g. 'time' OR a cell array of variables.
            %
            %
            % Output:
            %   v = the vector of data for that variable. If a cell array is given then each
            %       column in the returned array will correspond to the corresponding varName
            %
            % Example:
            %   m = auvmission(somemissiondir);
            %
            %   v = m.data('dataDC', 'time');
            %   u = m.data('dynamicControl', 'time') % does same as above line
            %
            %   g = m.data('gps', {'latitude', 'longitude'}) % returns an n x 2 matrix.
            %       % the first column is latitude, the second is longitude
            if isstr(varName)
                varName = {varName};
            end
            
            v = [];
            for i = 1:length(varName)
                n = varName{i};
                s = obj.datum(logName, n);
                if ~isempty(s)
                    if isempty(v)
                        v = ones(length(s.data), length(varName)) * NaN;
                    end
                    v(:, i) = s.data;
                end
            end
        end
        
        %%
        function n = logs(obj, useInst)
            % LOGS - Extract the ID/names used to access specific ROV logs
            %
            % Data from individual logs can be retrieved either by the logname (without the
            % .log) extension, or by using the instrument tag inside the log file. For
            % example, you can get data for the terrain nav log using either 'TerrainNav' or
            % 'ta.data'.
            %
            % This function pulls out the log names or instrument ids found in a
            % container.Map object created by loadauvlogs
            %
            % Usage:
            %   n = m.lognames(useInst)
            %
            % Inputs:
            %
            %   useInst = If false the log names are returned. If true, then the instrument
            %             ids are returned. Either can be used to access logs using the
            %             various 'get' functions [OPTIONAL! Default is false]
            %
            % Outputs:
            %   n = a cell array of strings
            %
            % Example:
            %   m = auvmission(someDirectory);
            %   n = m.lognames(m);
            %   n = m.lognames(m, false); % same as above
            %   n = m.lognames(m, true);
            if nargin == 1
                useInst = false;
            end
            
            %% Extract all unique log names from the Map. if useInst is true returned the
            % instrument names instead
            if useInst
                i = 0;
                ns = {};
                for k = obj.logmap.keys
                    i = i + 1;
                    r = obj.logmap(k{1});
                    % 'unique' in unlikely case that struct has more than one instrument id
                    ns{i} = unique({r.instrument});
                end
                n = [ns{:}]; % flatten cell array. In case struct has more than one id
            else
                n = obj.logmap.keys;
            end
        end
        
        %%
        function t = starttime(obj)
            % STARTTIME - Get the starting time of the mission.
            %
            % This function extracts the starting time of a mission by looking at both the
            % initial values in 'dataNav' and 'dataDC' 'time' variables and taking the max.
            %
            % Usage:
            %   t = m.starttime
            %
            % Output:
            %   t = The starting time of the mission. This is the raw value, so its units
            %       will be the same as the raw data.
            %
            nav = obj.data('dataNav', 'time');
            dc = obj.data('dataDC', 'time');
            t = max([min(nav) min(dc)]);
        end
        
        %%
        function v = datum(obj, logName, varName)
            % DATUM - Find a variable from logs loaded into Matlab
            %
            % Searches a map loaded by loadauvlogs for a specific variable and returns
            % the data array
            %
            % Usage:
            %   v = m.datum(logName, varName)
            %
            % Inputs:
            %
            %   logName = The name of the log from which a variable shoud be extracted. This
            %             should be one of the values found in 'logMap.keys'. You can
            %             also use the instrument name. For example, you can get
            %             dynamic control data using either 'dynamicControl' or
            %             'dataDC' as the logName.
            %
            %   varName = The name of the variable. e.g. 'time'
            %
            % Output:
            %   v = the structure of data for that variable. An empty struct is returned
            %       if no match is found
            %
            % Example:
            %   m = auvmission(somemissiondir);
            %   w = m.datum('dataDC', 'time');
            %   u = m.datum(dynamicControl', 'time'); % does same as above line
            %
            
            %% Extract the correct structures
            s = obj.log(logName);
            
            %% Extract the variable structure of interest
            v = struct([]);
            if isempty(s)
                warning(['Unable to find any data for log or instrument named ' logName])
            else
                i = strcmp(varName, {s.shortName});
                if any(i)
                    v = s(i);
                else
                    warning(['Unable to find the variable "' varName '" in the ' logName ' log'])
                end
            end
        end
        
        %%
        function add(obj, logName, s)
            % ADD - Adds a record structure to the data for a given log
            %
            % Usage:
            %  m.add(logName, s)
            %
            % Inputs:
            %  logName = the logname OR the instrument name to add a record to
            %  s = the structure to add. Structures should have the following
            %      fields (although this is not enforced): instrument, type,
            %      shortName, format, longName, units, size and data. The
            %      number of rows of the data will be checked against the
            %      existing records and will not be added if it is different
            %
            % Example:
            %  s = m.log('gps');
            %  s.shortName = 'foo';
            %  m.add('gps', s);
            k = obj.logname2mapkey(logName);
            v = obj.log(k);
            if ~isempty(v)
                if size(v(1).data, 1) == size(s.data, 1)
                    v(length(v) + 1) = s;
                    obj.logmap(k) = v;
                else
                    warning('AUV_MATLAB:auvmission:illegalArgument', ...
                        'The structure that you supplied is the wrong size');
                end
            else
                warning('AUV_MATLAB:auvmission:nomatch', ...
                    ['No log was found for "' logName '"']);
            end
        end
        
        %%
        function remove(obj, logName, varName)
            % REMOVE - Removes a record structure for a given log
            %
            % Usage:
            %   m.remove(logName, varName)
            %
            % Inputs:
            %  logName = The name of the log that a record will be remove from
            %  varName = The shortName of the recored to be removed.
            %
            % Example:
            %   m.remove('gps', 'latitude')
            k = obj.logname2mapkey(logName);
            data = obj.log(k);
            if ~isempty(data)
                vs = obj.variables(k);
                i = find(strcmp(varName, vs));
                if ~isempty(i)
                    data(i) = [];
                else
                    warning('AUV_MATLAB:auvmission:nomatch', ...
                        ['No match was found for "' varName '" for the log "' ...
                        logName '"']);
                end
                obj.logmap(k) = data;
            else
                warning('AUV_MATLAB:auvmission:nomatch', ...
                    ['No log was found for "' logName '"']);
            end
        end
        
        %%
        function [e, n, z] = utm(obj)
            % UTM - returns the GPS latitude and longitude as UTM
            %
            % Usage:
            %   [e, n, z] = m.utm
            %
            % Outputs:
            %   e = easting
            %   n = northing
            %   z = utm zone
            %
            lat = obj.data('gps', 'latitude');
            lon = obj.data('gps', 'longitude');
            % NOTE: The gps units are reported as degrees in the log file metadata but
            % they are really in radians
            [e, n, z] = deg2utm(rad2deg(lat), rad2deg(lon));
        end
        
        %%
        function vs = variables(obj, logName)
            % VARIABLES - Extract the variable names for the specified log
            %
            % Usage:
            %   vs = m.variables(logName)
            %
            % Inputs:
            %
            %   logName = the log name (without the .log extension) OR the instrument name
            %             (as defined in the log file)
            %
            % Output:
            %   vs = A cell array of all the variables from the given log/instrument
            %
            % Example:
            %   m = loadauvlogs(someDirectory)
            %   vs = auvlogvars(m, 'gps')
            r = obj.log(logName);
            vs = {};
            if ~isempty(r)
                vs = {r.shortName};
            end
        end
        
        %%
        function show(obj)
            % SHOW - Dump the contents from loadauvlogs out in easily readable form
            %
            % Usage:
            %   m.show
            %
            fprintf('Format: [instrument]: [shortName] ([type]), units are [units]\n')
            for k = obj.logmap.keys
                k = k{1}; % yank key out of the cell
                v = obj.logmap(k);
                fprintf(1, 'From file %s (%s.log)\n', k, k);
                for s = v
                    %s = s{1}; % yank structure out of cell
                    fprintf(1, '\t%s: %s (%s), units are %s\n', s.instrument, s.shortName, ...
                        s.type, s.units);
                end
            end
        end
        
        %%
        function s = find(obj, varName, searchAll)
            % FIND - Searches the mission for all variables that match a name
            %
            % Usage:
            %    s = m.find(searchTerm)
            %    s = m.find(searchTerm, searchAll)
            %
            % Inputs:
            %    searchTerm = the string to search for in all the records
            %       metadata.
            %
            %    searchAll = (boolean) If true (or not 0), then it searches the
            %       shortName, longName, and units fields for a string that
            %       contains the 'varName'. If false or 0 (the default), then
            %       it searches for records with shortNames that match exactly.
            %
            % Output:
            %    A struct array containing ALL records that have a matching
            %    short name
            if nargin < 3
                searchAll = 0;
            end
            
            s = struct([]);
            vs = obj.all;
            ok = 1;
            for i = vs
                if searchAll
                    j = [strfind(lower(i.shortName), lower(varName)) ...
                        strfind(lower(i.longName), lower(varName)) ...
                        strfind(lower(i.units), lower(varName))];
                else
                    j = strcmpi(varName, {i.shortName});
                end
                if any(j)
                    if ok
                        s = i;
                        ok = 0;
                    else
                        s = [s, i];
                    end
                end
                
            end
        end
        
        %%
        function s = all(obj)
            % ALL - Return all data in the mission as a struct array
            s = struct([]);
            i = 1;
            for k = obj.logmap.keys
                k = k{1}; % yank key of the cell
                if i
                    s = obj.logmap(k);
                    i = 0;
                else
                    s = [s, obj.logmap(k)];
                end
            end
            
        end
        
    end
    
    methods(Access = private)
        function k = logname2mapkey(obj, logName)
            k = '';
            if obj.logmap.isKey(logName)
                k = logName;
            else
                % If logName isnt' a log name look for 'logName' using the
                % 'instrument' field of the structures
                for kk = obj.logmap.keys  % r is a cell array with exactly 1 struct array in it
                    kk = kk{1};
                    r = obj.logmap(kk);
                    n = r(1).instrument;
                    j = strcmp(logName, n);
                    if any(j)
                        k = kk;
                        break
                    end
                end
            end
        end
    end
    
end