function trn_gendiveresults(sourceDir, targetdir, suffix)

if ~exist(targetdir, 'dir')
    warning('AUV-MATLAB:trn_gendiveresults:missingdirectory', ...
        ['The directory: ' targetdir ' does not exist. Creating it.']);
    mkdir(targetdir);
end

if nargin == 2
    suffix = '';
end

am = auvmission(sourceDir);
m = trn_loaddata(am);
trn_parsedivedata(m, targetdir, suffix)




