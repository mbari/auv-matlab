function f = dir2(d)
% DIR2  - Get the name of all specified files in a d
%
% Use as: f = dir2(d)
%
% Inputs: d = Includes directory to look in and filename filter
%                (Default = '/*.m')
%
% Output: f = cell array of filenames

% B. Schlining
% 1997-07-10
% 2013-04-25 - Change output to return a cell array. Renamed from getfname to dir2


if nargin < 1
   d = '*.m';
end

fDat = dir(d);
[r c] = size(fDat);

if r == 0
   f = {};
else
   f = {fDat.name};
end

