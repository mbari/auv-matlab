function auv_navplot(mission)
  figure;
  s.logname = {'ta.data', 'ta.data', 'ta.data', 'gps'};
  s.xName = {'ta.navE', 'ta.mleE', 'ta.mmseE', 'easting'};
  s.yName = {'ta.navN', 'ta.mleN', 'ta.mmseN', 'northing'};
  s.marker = {'r.', 'y.', 'g.', 'b.'};
  for i = 1:length(s.logname)
    plotit(mission, s.logname{i}, s.xName{i}, s.yName{i}, s.marker{i})
  end
  axis('equal')
  legend(s.xName)
end

function plotit(mission, logname, xName, yName, marker)
  x = mission.datum(logname, xName);
  y = mission.datum(logname, yName);
  if ~isempty(x) && ~isempty(y)
    hold('on')
    plot(x.data, y.data, marker)
    hold('off')
  else
    warning('AUV-MISSION:auv_navplot:notfound', ...
      ['Did not find both "' xName '" and "' yName '" for log ' logname]);
  end
  
end
  