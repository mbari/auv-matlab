function trn_parsediveddata(m, targetDir, suffix)

if nargin == 2
    suffix = '';
end
b = 'EMPTY'; % blank variable 

%% Initialize conditional parameters
starttime = m.starttime;
startsdn = utc2sdn(starttime);
isbefore2009 =  startsdn < datenum('01-Jan-2009');
isbefore2010 = startsdn < datenum('18-Nov-2010');
haskearfott = ~isempty(m.variables('kearfott'));



%% create measData.txt
if isbefore2009
    tString = 'ta.mmseTime';
else
    tString = 'ta.dvlTime';
end
varsOfInterest = {b, tString, ...
    b, b, b, b, b, b, b, b, b, b, b, b, b, b, ... 
    'ta.beam(1)', 'ta.beam(2)', 'ta.beam(3)', 'ta.beam(4)', ...
    'ta.dvlStatus(1)', 'ta.dvlStatus(2)', 'ta.dvlStatus(3)', 'ta.dvlStatus(4)'};
warning off
measData = m.data('ta.data', varsOfInterest);
warning on
measData(isnan(measData)) = 0;
measData(:, 1) = 1;
header = strjoin(varsOfInterest, ' ');
writetxt(header, measData, targetDir, 'measData', suffix)


%% create dataKft.txt
if isbefore2009
    tString = 'ta.mmseTime';
else
    tString = 'ta.dvlTime';
end
varsOfInterest = {tString, ...
    'ta.dvlValid', 'ta.gpsValid', 'ta.bottomLock', b, b, ...
    'ta.navN', 'ta.navE', 'ta.depth', 'ta.roll', 'ta.pitch', 'ta.yaw', ...
    'ta.dvlVelocity(1)', 'ta.dvlVelocity(2)', 'ta.dvlVelocity(3)', ...
    b, b, b, 'ta.omega(1)', 'ta.omega(2)', 'ta.omega(3)', b};
warning off
dataKft = m.data('ta.data', varsOfInterest);
warning on
dataKft(isnan(dataKft)) = 0;
header = strjoin(varsOfInterest, ' ');
writetxt(header, dataKft, targetDir, 'dataKft', suffix)


%% create measData_all
varsOfInterest = {'', 'time', ...
    b, b, b, b, b, b, b, b, b, b, b, b, b, b, ...
    'dvlBeam1', 'dvlBeam2', 'dvlBeam3', 'dvlBeam4', ...
    b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, ...
    b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, ...
    b, b, b, b, b, b,b};
warning off
measData_all = m.data('dataDvl', varsOfInterest);
warning on
measData_all(isnan(measData_all)) = 0;
measData_all(:, [1, 23:26]) = 1;
if isbefore2010
    measData_all(:, 17:20) = (1.0 / cos(30.0 * pi / 180.0)) * measData_all(:,17:20);
end
header = strjoin(varsOfInterest, ' ');
writetxt(header, measData_all, targetDir, 'measData_all', suffix)


%% create dataKft_all
varsOfInterest = {'time', 'mDvlValid', 'mGpsValid', ...
    b, b, b, 'mPos_x', 'mPos_y', 'mDepth', 'mPhi', 'mTheta', 'mPsi', ...
    b, b, b, b, b, b, 'mOmega_x', 'mOmega_y', 'mOmega_z', b};
warning off
dataKft_all = m.data('dataNav', varsOfInterest);
warning on
if haskearfott
    % for MAUV data sets, the velocity is recorded in a different frame where z
    % is up.  to correct, have to switch the sign of the y and z components
    dataKft_all(:, 14:15) = -dataKft_all(:, 14:15);
end
% Interpolate (future nearest neighbor) dvl water velocity to nav times
t = m.data('dataDvl', 'time');
ti = m.data('dataNav', 'time');
try 
    dvlWatVelx = interpfuture(t, m.data('dataNav', 'dvlWatVelx'), ti);
    dvlWatVely = interpfuture(t, m.data('dataNav', 'dvlWatVely'), ti);
    dvlWatVelz = interpfuture(t, m.data('dataNav', 'dvlWatVelz'), ti);
    dvlWatVelStat = interpfuture(t, m.data('dataNav', 'dvlWatVelStat'), ti);
    dvlBotVelStat = interpfuture(t, m.data('dataNav', 'dvlBotVelStat'), ti);
    dataKft_all(:, 4) = dvlBotVelStat;
    dataKft_all(:, 13:15) = [dvlWatVelx dvlWatVely, dvlWatVelz];
catch me
    warning('AUV-MATLAB:trn_parsediveddata:missingdata', ...
        ['Unable to load water velocities: ' me.message]);
end
header = strjoin(varsOfInterest, ' ');
writetxt(header, dataKft_all, targetDir, 'dataKft_all', suffix);

%% create gps
warning off
gps = m.data('gps', {'time', '', ''});
warning on
[e n] = m.utm;
gps(:, 2:3) = [n e];
header = 'time,northing,easting';
writetxt(header, gps, targetDir, 'gps', suffix)

end

function writetxt(header, v, targetDir, n, suffix)
if length(suffix) > 0
    f = [n '_' suffix '.txt'];
else
    f = [n '.txt'];
end
v(isnan(v)) = 0; % Set NaN's to 0 so that trn can read them

d = fullfile(targetDir, f);
fprintf(1, 'Writing %s\n', d);
fid = fopen(d, 'wt');
fprintf(fid, '#%s\n', header);
fclose(fid);
dlmwrite(d, v, 'delimiter', ' ', 'precision', '%f', '-append');
%save(d, 'v', '-ASCII', '-DOUBLE')
end
