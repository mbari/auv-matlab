function ctd = navctdmerge(directory)
% navctdmerge - Read AUV mission logs and interpolate position to CTD
%
% Usage:
%   c = navctdmerge(missionDirectory);
%
% Inputs:
%   missionDirectory = An AUV directory containing the kearfottt and
%   fastcatlog log files.
%
% Outputs:
%   c = An N by 6 array. Each row is a sample in a timeseries. The columns
%   are: time, latitude, longitude, salinity, temperature, pressure. Time
%   is matlab formatted time (See datenum)
%
% Example:
%   c = navctdmerge('/Users/brian/AUV/missionlogs/GulfOfCalifornia/2015.055.00');
%   plot(c(:, 3), c(:, 2), '.');
%   axis('equal');

% Brian Schlining
% 2019-04-17

logNames = {'navigation.log', 'kearfott.log', 'fastcatlog.log'};
missionLogs = containers.Map;
for f = logNames
    l = fullfile(directory, char(f));
    fprintf(1, 'Reading %s\n', l);
    try
        v = readauvlog(l);
        n = stringtokenizer(char(f), 1, '.');
        missionLogs(n) = v;
    catch
        warning(['Failed to read ' l])
    end
end

m = auvmission(missionLogs);
nav = m.data('kearfott', {'time', 'mLonK', 'mLatK'});
[~, good, ~] = unique(nav(:, 1));
nav = nav(good, :);

ctd = m.data('fastcatlog', {'time', 'calculated_salinity', 'temperature', 'pressure'});
[~, good, ~] = unique(ctd(:, 1));
ctd = ctd(good, :);
lon = interp1(nav(:, 1), nav(:, 2), ctd(:, 1));
lat = interp1(nav(:, 1), nav(:, 3), ctd(:, 1));

ctd = [utc2sdn(ctd(:, 1)) lat lon ctd(:, 2:end)];
% d = array2table(ctd, 'VariableNames', {'time', 'latitude', 'longitude', 'salinity', 'temperature', 'pressure'});
