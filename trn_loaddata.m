function newMission = trn_loaddata(mission)
% TRN_LOADDATA - Premunges an AUV mission.
%
% trn_loaddata does the following to a mission:
%  1. Deletes any records that occur before auvmission.starttime
%  2. Adds a new 'elapsedTime' record to each log in the auvmission

% Brian Schlining
% 2014-02-10

% Synchronize the start times in all of the data files.  Discard data that
% begins before the start time.
starttime = mission.starttime;
startsdn = utc2sdn(starttime);
isbefore2009 =  startsdn < datenum('01-Jan-2009');
isbefore2010 = startsdn < datenum('18-Nov-2010');
haskearfott = ~isempty(mission.variables('kearfott'));

% This loop just chops off all data that occurs before 'starttime'
% and creates a new auvmission object
map = containers.Map();
for key = mission.logmap.keys
    logname = key{1};             % flatten cell
    t = mission.data(logname, 'time'); % time
    good = t >= starttime;
    logdata = mission.log(logname);  % All records for logname as a struct
    for i = 1:length(logdata)
        d = logdata(i);         % A single record as a struct
        d.data = d.data(good);
        if exist('dEdited', 'var')
            dEdited(i) = d;
        else
            dEdited = d;
        end
    end
    map(logname) = dEdited;
    clear('dEdited')
end
newMission = auvmission(map);
newMission.mission = mission.mission;

%% Add 'elapsedtime' log record to each log structure
for key = newMission.logs
    logname = key{1}; % flatten cell
    elapsedTime = newMission.datum(logname, 'time');
    elapsedTime.data = elapsedTime.data - starttime;
    elapsedTime.shortName = 'elapsedtime';
    elapsedTime.longName = 'elapsedtime';
    elapsedTime.units = 'seconds';
    newMission.add(logname, elapsedTime);
end

%% Create a sampleTime variable for ta.data. 
% This is the timestamp that should used when working with ta.data
logdata = newMission.log('ta.data');
if ~isempty(logdata)
    if isbefore2009
        timeVar = 'ta.mmseTime';
    else
        timeVar = 'ta.dvlTime';
    end
    sampleTime = newMission.datum('ta.data', timeVar);
    sampleTime.shortName = 'sampleTime';
    mission.add('ta.data', sampleTime);
end

%% Munge DVL data for dates before 2010
logdata = newMission.log('ta.data');
if ~isempty(logdata)
    if isbefore2010
        for k = {'dvlBeam1', 'dvlBeam2', 'dvlBeam3', 'dvlBeam4'}
           d = newMission.datum('ta.data', k{1});
           d.data = (1.0 / cos(30.0 * pi / 180.0)) * d.data;
           d.notes{length(d.notes) + 1} = 'Data is before 2010. Applied: (1.0 / cos(30.0 * pi / 180.0)) * data';
           newMission.remove('ta.data', k{1});
           newMission.add('ta.data', d);
        end
    end
end

%% If needed, flip velocity
% for MAUV data sets, the velocity is recorded in a different frame where z
% is up.  to correct, have to switch the sign of the y and z components
if haskearfott
    % ???
end

%% Add UTM to gps
[e n z] = newMission.utm;
east = newMission.datum('gps', 'longitude');
east.shortName = 'easting';
east.longName = 'Vehicle UTM Easting';
east.units = 'meters';
east.notes = {};
east.data = e;
newMission.add('gps', east);
north = newMission.datum('gps', 'latitude');
north.shortName = 'northing';
north.longName = 'Vehicle UTM Northing';
north.units = 'meters';
north.notes = {};
north.data = n;
newMission.add('gps', north);
zone.instrument = 'gps';
zone.type = 'char';
zone.shortName = 'utmZone';
zone.format = '%s';
zone.longName = 'Vehicle UTM Zone';
zone.notes = {};
zone.units = '';
zone.size = 4;
zone.data = z;
newMission.add('gps', zone);

newMission.notes{length(newMission.notes) + 1} = 'trn_loaddata applied';

