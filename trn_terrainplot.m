function trn_terrainplot(m, grd, targetDir, suffix)

% check if attitude angle field exists
estimateAlititude = any(strcmp(m.variables('ta.data', ta.mmsePhi'), 'ta.mmsePhi'));

%% Extract variables of intereste
xTrue = m.data('ta.data', {'ta.navN', 'ta.navE', 'ta.depth'});
tercomEst = m.data('ta.data', ...
    {'ta.mleN', 'ta.mleE', 'ta.mleZ', 'ta.mlePhi', 'ta.mleTheta', 'ta.mlePsi'});
mmseEst = m.data('ta.data', ...
    {'ta.mmseN', 'ta.mmseE', 'ta.mmseZ', 'ta.mmsePhi', 'ta.mmseTheta', 'ta.mmsePsi'});
tercomCov = m.data('ta.data', ...
    {'ta.mmseVarN', 'ta.mmseVarE', 'ta.mmseVarZ', 'ta.mmseVarPhi', 'ta.mmseVarTheta', 'ta.mmseVarPsi'});
t = m.data('ta.data', 'ta.navTime');
dvlBeams = m.data('ta.data', {'ta.beam(1)', 'ta.beam(2)', 'ta.beam(3)', 'ta.beam(4)'});
dvlStatus = m.data('ta.data', {'ta.dvlStatus(1)', 'ta.dvlStatus(2)', 'ta.dvlStatus(3)', 'ta.dvlStatus(4)'});
attitude = m.data('ta.data', {'ta.roll', 'ta.pitch', 'ta.yaw'});
dvlVelocities = m.data('ta.data', {'ta.dvlVelocity(1)', 'ta.dvlVelocity()', 'ta.dvlVelocity(3)'});
dvlValid = m.data('ta.data', 'dvlValid');
gpsValid = m.data('ta.data', 'gpsValid');


indices = [1:length(t)]';
a = find(mmseEst(:, 2) > 0, 1, 'first');
indices = indices(a:end);
x = t(indices);

homerTime = 5;
navTime0 = m.data('ta.data', 'elapsedtime');
[homerTrue(2,1), homerTrue(1,1), utmzone] = deg2utm(36.8161143, -121.9835191);
[homerEst(2,1), homerEst(1,1), utmzone] = deg2utm(36.8164934,-121.9835814);
[estHomerLocUTM(2), estHomerLocUTM(1), utmzone] = deg2utm(36.8162810, -121.9835692);
