function trn_gpsanalysis(m, targetDir, suffix)

%% Initialize data we need
t = m.data('gps', 'time');
[e n z] = m.utm;
tn = m.data('dataNav', 'time');
mPos_x = m.data('dataNav', 'mPos_x');
mPos_y = m.data('dataNav', 'mPos_y');
hdop = m.data('gps', 'hdop');
numberOfSatellites = m.data('gps', 'numberOfSatellites');
savePlots = nargin >= 2;
if nargin < 3
    suffix = '';
end

%% find resurfacing type and gps position
dt = diff(t);
good = find(dt > 100);
if isempty(good)
    warning('No GPS data at vehicle resurfacing')
end

resurfaceTime = t(good + 1);
diveTime = t(good);
resurfacePose = [n(good + 1), e(good + 1)];
divePose = [n(good), e(good)];

%% Determine navigation offset from gps at vehicle resurfacing
resurfaceINSPose = ones(length(good), 2) * NaN;
diveINSPose = ones(length(good), 2) * NaN;
for i = 1:length(good)
    iNav = find(tn >= resurfaceTime(i), 1, 'first');
    resurfaceINSPose(i, :) = [mPos_x(iNav - 1) mPos_y(iNav - 1)];

    iNav2 = find(tn <= diveTime(i), 1, 'last');
    diveINSPose(i, :) = [mPos_x(iNav2) mPos_y(iNav2)];
end
insOffset = resurfacePose - resurfaceINSPose;
insOffsetDive = divePose - diveINSPose;

%% valid nav data indices
validIdx = intersect(find(mPos_x > 0), find(mPos_y > 0));

%% Plot navData vs GPS data
figure
set(gcf, 'Name', 'GPS: Navigatino offset at surface', ...
    'color', 'w');
plot(mPos_y(validIdx), mPos_x(validIdx), '.--');
hold on
plot(e, n, 'k.-');
plot(divePose(:, 2), divePose(:, 2), 'g*');
plot(resurfacePose(:, 2), resurfacePose(:, 2), 'r*');
legend('INS vehicle path', 'GPS track', 'GPS pose at vehicle dive', ...
    'GPS pose at vehicle resurface');%, 'Location','NorthWest');
xlabel('Eastings (m)');
ylabel('Northings (m)');
title(strcat('GPS and Inertial vehicle tracks, for ', suffix));
axis equal;
grid on;
offsets = cell(length(good) + 1,1);
offsets{1}  = 'INS offset from GPS at resurface:';
for i = 1:length(good)
   offsets{i+1} = strcat('Leg #',num2str(i),': North: ', num2str(insOffset(i, 1)), 'm, East: ',num2str(insOffset(i, 2)), 'm');   
end
annotation('textbox',[.15 .2 .5 .1 * length(good)], 'String', offsets);
if (savePlots) 
    save2pdf(fullfile(targetDir, ['gpsWithINSTracks_' suffix, '.pdf']), gcf, 400);
end

%% Plot gps data accuracy information
figure;
set(gcf, 'Name', 'GPS: HDOPs', ...
    'color', 'w');
plot(hdop, numberOfSatellites, 's', 'MarkerSize',6, 'MarkerFaceColor','b');
ylabel('# of Satellites');
xlabel('HDOP');
title(['GPS Accuracy Information, for ' suffix]);
hold on;
h1 = plot(hdop(good + 1), numberOfSatellites(good + 1), 'ro', ...
    'MarkerSize', 11, 'LineWidth', 2);
h2 = plot(hdop(good), numberOfSatellites(good), 'go', ...
    'MarkerSize', 14,'LineWidth', 2);
legend([h1 h2], 'Resurface Fix', 'Dive Fix');
if (savePlots)
    save2pdf(fullfile(targetDir, ['gpsAccuracy_' suffix '.pdf'], gcf, 400))
end








