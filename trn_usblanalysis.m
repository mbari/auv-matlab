function trn_gpsanalysis(m, targetDir, suffix)

%% Initialize data we need
sonardyneAcceptance = auvlogdata(m, 'usbllog', 'sonardyneAcceptance');
sonardyneDetectLevel = auvlogdata(m, 'usbllog', 'sonardyneDetectLevel');
sonardyneDoppler = auvlogdata(m, 'usbllog', 'sonardyneDoppler');
sonardyneQuality = auvlogdata(m, 'usbllog', 'sonardyneQuality');
sonardyneRange = auvlogdata(m, 'usbllog', 'sonardyneRange');
sonardyneSigLevel = auvlogdata(m, 'usbllog', 'sonardyneSigLevel');
sonardyneStatus = auvlogdata(m, 'usbllog', 'sonardyneStatus');
sonardyneX = auvlogdata(m, 'usbllog', 'sonardyneX');
sonardyneY = auvlogdata(m, 'usbllog', 'sonardyneY');
sonardyneZ = auvlogdata(m, 'usbllog', 'sonardyneZ');
t = auvlogdata(m, 'usbllog', 'time');
et = auvlogdata(m, 'usbllog', 'elapsedtime');

savePlots = nargin >= 2;
if nargin < 3
    suffix = '';
end

%% Find minimum range to the beacon - i.e., when vehicle is directly above
% AND where the range status is GOOD (status = 2)
% AND where sonardyneZ > 0 (should never be at or below it)
indx = find((sonardyneStatus == 0) | (sonardyneStatus == 3));
indxx = find(sonardyneRange < 250 & sonardyneRange > 15 & ...
    sonardyneStatus == 1 & sonardyneZ > 0 & sonardyneAcceptance == 8 & ...
    sonardyneQuality < 0.0015);
diffVal = diff(indxx);
diffVal2 = [0; diffVal(1:end - 1)];
valid = find(diffVal == 1 | diffVal2 == 1);
indxx = indxx(valid);
[a b] = min(sonardyneRange(indxx));
idx_minRange = indxx(b);

%% Plot USBL range to the beacon vs. time
f1 = figure;
set(f1, 'Name', 'USBL Homing Range to Beacon', 'color', 'w');
subplot(2, 1, 1);
set(gca, 'fontsize', 12);
stairs(et, [sonardyneRange sonardyneX sonardyneY sonardyneZ]);
hold on
plot(et, sonardyneRange, 'xb')
plot(et, sonardyneX, 'xg')
plot(et, sonardyneY, 'xr')
plot(et, sonardyneZ, 'xc')
plot_lines(et(idx_minRange), [], 'k-')
grid on

title(['Dive #' suffix ', Usbl R=Blue;X=Green;Y=Red;Z=Cyan']);
legend('range','North', 'East', 'Depth');
ylabel('Range, Meters')

subplot(2, 1, 2);
set(gca, 'fontsize', 12)
deltaT = [diff(t); 0];
stairs(et, deltaT);
hold on
indxx = find(sonardyneStatus == 0);
plot(et(indxx), deltaT(indxx), 'xc')
indxx = find(sonardyneStatus == 1);
plot(et(indxx), deltaT(indxx), 'xg')
indxx = find(sonardyneStatus == 2);
plot(et(indxx), deltaT(indxx), 'xb')
indxx = find(sonardyneStatus == 3);
plot(et(indxx), deltaT(indxx), 'xr');
plot_lines(et(idx_minRange), [], 'k-');
title( ['Ping Interval; Cyan=Undefined;Green=All;Blue=Range Only;Red=Beacon ' ...
       'Not Detected'] )
ylabel('Seconds');
xlabel('Seconds');
annotation('textbox',[.15 .2 .1 .1], 'String', {'Closest Approach to Homer:';strcat('Time: ',num2str(t(idx_minRange)), 'sec');...
       strcat('Range:   ', num2str(sonardyneRange(idx_minRange)), 'm')}, 'BackgroundColor', 'w');
zoom on
grid on
if savePlots
    save2pdf(fullfile(targetDir, ['usblHomingResults_' suffix '.pdf']), gcf, 400);
end


%% Plot USBL accuracy
f2 = figure;
set(f2, 'Name', 'USBL Homing Accuracy', 'color', 'w');

subplot(4, 1, 1)
set(gca,'fontsize', 12);
stairs(et, [sonardyneSigLevel sonardyneDetectLevel])
hold on
h1 = plot(et, sonardyneSigLevel, 'xb');
h2 = plot(et, sonardyneDetectLevel,'xg');
legend([h1, h2],'Signal level', 'Detection level');
plot_lines(et(idx_minRange), [], 'k-');
title('Signal Level and Signal-to-Noise Ratio')
ylabel('Decibels')
grid on

subplot( 4, 1, 3 )
set(gca,'fontsize',12);
stairs(et, sonardyneDoppler);
hold on;
plot_lines(et(idx_minRange), [], 'k-');
title('Doppler Offset of Signal from Carrier Frequency')
ylabel('Hz')
grid on

subplot( 4, 1, 4 )
set(gca,'fontsize',12);
stairs(et, sonardyneAcceptance);
hold on;
plot_lines(et(idx_minRange), [], 'k-');
title('Acceptance: Number of Good Slices Through Pulse Signal')
ylabel('N/A')
xlabel('Seconds')
grid on

subplot( 4, 1, 2 )
set(gca,'fontsize',12);
stairs(et, sonardyneQuality);
hold on;
plot_lines(et(idx_minRange), [], 'k-');
title('Quality (Least Squares Residual Error)')
ylabel('N/A')
grid on
if (savePlots)
    save2pdf(fullfile(targetDir, ['usbleHomingAccuracy_' suffix '.pdf']), gcf, 400);
end

%% Save valid sonardyne hits to file
if (savePlots)
    validSonardyne = [t(indxx) sonardyneX(indxx) sonardyneY(indxx) sonardyneZ(indxx)];
    save(fullfile(datadir, ['sonardyneData_' suffix '.txt']), 'validSonardyne', ...
        '-ASCII', '-DOUBLE');

    %% Save all sonardyne data
    allSonardyne = auvlogdata(m, 'usbllog', auvlogvars(m, 'usbllog'));
    save(fullfile(datadir, ['dataSD_' suffix '.txt']), 'allSonardyne', ...
        '-ASCII', '-DOUBLE');

    %% Write sizes
    fid = fopen(fullfile(datadir, 'README.txt'), 'at');
    fprintf(fid, '\n%i \t 4 \t sonardyneData_%s.txt', size(validSonardyne, 1), suffix);
    fprintf(fid, '\n%i \t 13 \t dataSD_%s_%s.txt\n', size(allSonardyne, 1), suffix);
    fclose(fid);
end
%% Save index of closest homer approach in text file
%homerTime = t(idx_minRange);
%navTime0


