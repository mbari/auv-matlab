function yi = interpfuture(x, y, xi)
% INTERPFUTURE - Nearest neighbor interp using 'future' x values only
%
% Usage:
%   yi = interpfuture(x, y, xi)
%
% Inputs:
%   x = the independant variable
%   y = the dependant variable
%   xi = the x values to interpolate to
%
% Ouput:
%   yi = Nearest neighbor interpolated y values at xi. The values use 'future'
%        interpolation in that only values of y at x(i) >= xi(i) are used.
%        xi values above max(x) will return as NaN

% Brian Schlining
% 2013-05-02

% TODO check x and y should be vectors only

yi = ones(size(xi)) * NaN;
for i = 1:length(xi)
    dx = x - xi(i);
    good = min(find(dx >= 0));
    if ~isempty(good)
        yi(i) = y(good);
    end
end