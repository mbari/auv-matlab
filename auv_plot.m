function auv_plot(mission)
  figure;
  x = mission.datum('navigation', 'mPos_x').data;
  y = mission.datum('navigation', 'mPos_y').data;
  z = mission.datum('navigation', 'mDepth').data;
  patch([x; nan],[y; nan],[z; nan],[z; nan],'EdgeColor','interp','FaceColor','none');
  axis('equal');
  grid('on');
  set(gca, 'ZDir', 'reverse');
  view(3);
  title(mission.mission);
  colorbar;
end