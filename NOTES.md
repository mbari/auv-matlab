# Notes

## Data Quirks
- TerrainNav.log (ta.data) has utm positions as ta.mleE/N and ta.navN/E. However, there is no utm zone information stored anywhere! __I need that info to convert it back to geographic degrees.__
- gps.log stores latitude and longitude as radians. __Need to convert them to degrees__
- dataNav has UTM zone for 2011.137.01 listed as 10S. This is __WRONG_, it's actually 10N. This is likely true for other easting/northing variables.