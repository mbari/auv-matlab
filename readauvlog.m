function data = readauvlog(logname)
% READAUVLOG - Read an AUV log data file.
%
% Use as: data = readauvlog(logname)
%
% Inputs: logname = The AUV data log filename to read. (i.e. ctdDriver.log, 
%       biolume.log, etc.)
%
% Output: struct array including metadata. Each value in the array is a struct
%       with the fields:
%           instrument - as definied in the first line of the log file
%           type - describes the datatype as defined in the lob, mapped to matlab's types
%           shortName - A short descriptive name 
%           format - A formating string for the data
%           longName - A longer name. Usually this is the same as the short name
%           units - The units of this data record
%           size - The size in bytes of this data type
%           data - An n by 1 array of the data for this record
%
%       All fields are populated with data read from the log file.
%
% Example:
%   data = readauvlog('gps.log');


% Brian Schlining
% 2007-06-15

%% Open the log file, get the device name from the header
fid = fopen(logname, 'r', 'l');
s = fgetl(fid); 
instrumentName = stringtokenizer(s, 3);

%% Parse the ASCII header of the log file for metadata
i = 0;
while(~feof(fid))
    s = fgetl(fid);  
    i = i + 1;
    if (strcmp(stringtokenizer(s, 2), 'begin'))
        break    
    end
    
    % Parse the variable info
    data(i).instrument = instrumentName;
    data(i).type = stringtokenizer(s, 2);
    data(i).shortName = stringtokenizer(s, 3);
    data(i).format = stringtokenizer(s, 4);
    data(i).longName = stringtokenizer(s, 2, ',');
    data(i).notes = {};
    
    switch data(i).shortName
        case 'time'
            data(i).units = 'seconds since 1970-01-01 00:00:00Z';
        otherwise         
            data(i).units = stringtokenizer(s, 3, ',');
    end
    
    % Map the dataformats correctly   
    switch lower(data(i).type)
        case {'float'}
            data(i).type = 'float32'; 
            data(i).size = 4;
        case {'integer'}
            data(i).type = 'int32';
            data(i).size = 4;
        case {'short'}
            data(i).type = 'int16';
            data(i).size = 2;
        otherwise
            data(i).type = 'double'; 
            data(i).size = 8;
    end
end

%% Parse the binary data section
% Pre allocate memory
byteStart = ftell(fid);
fseek(fid, 0, 'eof');
byteEnd = ftell(fid);
fseek(fid, byteStart, 'bof');
nBytes = byteEnd - byteStart;
recordLength = 0;
for i = 1:length(data)
    recordLength = recordLength + data(i).size;
end
nRecords = floor(nBytes/recordLength);
for i = 1:length(data)
    data(i).data = ones(1, nRecords) * NaN;
end

% Read the data from the log
for i = 1:length(data)
    byteStart = ftell(fid);
    data(i).data = fread(fid, nRecords, data(i).type, recordLength - data(i).size);
    fseek(fid, byteStart + data(i).size, 'bof');
end
fclose(fid);



