function y = rad2deg(x) 
% rad2deg - Converts angle in radians to degrees 
%  
% Use As:   rad2deg(angle) 
% 
% Input:    angle in radians 
% Output:   angle in degrees 
% 
% Example:  rad2deg(3 * pi / 2) -> 270 

% Brian Schlining
% 2013-04-25

y = x * 180 / pi;  
